

This RPG has been made as part of my iOs developper course from OpenClassrooms.

Here are the constraints:

  1. First project: Create a RPG in CLI.

  2. Initializing the game by asking each player to choose characters for their team.  
  Each player would have to choose an unique name for each character.

  3. It's a turn-based game. Player 1 choose a character from his team.  
  Then choose his target (from his team if it's a heal, from opponant team if it's an attack).  
  Realise the action. Check if the game is over, otherwise it's player 2 turn.

  4. When game is over (all characters fromm all opponants are dead), display the winner and some game statistics.

  5. The game is composed of 2 players. Each players have a team of 3 characters.  
  Characters must have health points, a name and a weapon.  

  6. Add a random chest containing a weapon, which can pop at each character turn.  

I did a little bit more than what was asked :

  1. You can set as many players and as many characters as you want, the game still work the same way.

  2. Player have to choose their name too. 

  3. Re-play functionality.

